#!/usr/bin/perl
use open ':std', ':encoding(UTF-8)';
use utf8;

eval "use Encode::Locale";
if ($@) {
	if ((${^UNICODE} & 32) == 0 and join(' ',@ARGV) =~ /[^[:ascii:]]/) { die q{
Eraro: `Encode::Locale` ne instalita. Uzu nur flagojn ASCII aŭ alvoku `perl -CA tru.pl`.
Error: `Encode::Locale` not installed. Use only ASCII flags or invoke `perl -CA tru.pl`.
}}}
else {
	Encode::Locale::decode_argv();
}
use Getopt::Long qw(GetOptions :config no_ignore_case bundling);
sub HELP_MESSAGE { die q{version:θ
Transliteri inter Esperanto kaj aliaj ortografioj.
Transliterate between Esperanto and other orthographies.

Minusklaj flagoj transliteras el Esperanto;
Majuskloj flagoj transliteras al Esperanto.
Lowercase flags transliterate from Esperanto;
Uppercase flags transliterate to Esperanto.

-cC            Cirila/Cyrillic
-gG            Grekeca/Greekish
--gl --go      Glagolica/Glagolitic
-hH            H-sistemo/H-system
--ux           H-sistemo kun 'ux' / H-system with 'ux'
-iI --ipa      IPA - Internacia Fonetika Alfabeto / International Phonetic Alphabet
--sam          SAM Atari 400/800 - Aŭtomatika Buŝo el Programaro / Software Automatic Mouth
-ŝŜ --sh --sx  Ŝavia/Shavian
--senŭ --senw  Ŝavia (sen 'ŭ' diftongoj) / Shavian (no 'ŭ' diphthongs)
-xX            X-sistemo/X-system
}}
my %opts;
GetOptions(\%opts,'c','C','g','G','gl|go','GL|GO','h','H|UX','ux','i|ipa','I|IPA','sam','SAM','ŝ|sh|sx','Ŝ|SH|SX|SENŬ|SENW','senŭ|senw','x','X')
	or HELP_MESSAGE();

# Table of Greek diacritic vowels
my %gd = (
	Α => Ά,
	Ε => Έ,
	Ι => Ί,
	Ο => Ό,
	Υ => Ύ,
	α => ά,
	ε => έ,
	ι => ί,
	ο => ό,
	υ => ύ
);

die "Not implemented.\n" if $opts{SAM};

while (<>) {
	print(tr/AaBbCcĈĉDdEeFfGgĜĝHhĤĥIiJjĴĵKkLlMmNnOoPpRrSsŜŝTtUuŬŭVvZzẐẑ/АаБбЦцЧчДдЕеФфГгЏџҺһХхИиЙйЖжКкЛлМмНнОоПпРрСсШшТтУуЎўВвЗзЅѕ/r) if ($opts{c});
	print(s/Я/Ja/gr =~ s/я/ja/gr =~ s/Є/Je/gr =~ s/є/je/gr =~ s/Ї/Ji/gr =~ s/ї/ji/gr =~ s/Ё/Jo/gr =~ s/ё/jo/gr =~ s/Ю/Ju/gr =~ s/ю/ju/gr =~ tr/АаБбЦцЧчЋћДдЕеЭэФфГгҐґЏџЂђЃѓҺһХхИиІіЫыЙйЈјЖжКкЛлЉљМмНнЊњОоПпРрСсШшЩщТтУуЎўВвЗзЅѕ/AaBbCcĈĉĈĉDdEeEeFfGgGgĜĝĜĝĜĝHhĤĥIiIiIiJjJjĴĵKkLlLlMmNnNnOoPpRrSsŜŝŜŝTtUuŬŭVvZzẐẑ/r) if ($opts{C});
	print(tr/AaBbCcĈĉDdEeFfGgĜĝHhĤĥIiJjKkLlMmNnOoPpRrSsŜŝTtUuŬŭVvZz/ΑαΒϐͶͷͲͳΔδΕεΦφΓγϪϫͰͱΧχΙιͿϳΚκΛλΜμΝνΟοΠπΡρΣσϷϸΤτΥυϜϝꞴꞵΖζ/r =~ s/([ΑαΕεΙιΟοΥυ])(?=[^ΑαΕεΙιΟοΥυ ]*[ΑαΕεΙιΟοΥυ][^ΑαΕεΙιΟοΥυ ]*\s)/$gd{$1}/gr) if ($opts{g});
	print(tr/ΑαΆάΒϐͶͷͲͳΔδΕεΈέΦφΓγϪϫͰͱΧχΙιΊίͿϳΚκΛλΜμΝνΟοΌόΠπΡρΣσϷϸΤτΥυΎύϜϝꞴꞵΖζ/AaAaBbCcĈĉDdEeEeFfGgĜĝHhĤĥIiIiJjKkLlMmNnOoOoPpRrSsŜŝTtUuUuŬŭVvZz/r) if ($opts{G});
	print(tr/AaBbCcĈĉDdEeFfGgĜĝHhĤĥIiJjĴĵKkLlMmNnOoPpRrSsŜŝTtUuŬŭVvZz/ⰀⰰⰁⰱⰜⱌⰝⱍⰄⰴⰅⰵⰗⱇⰃⰳⰌⰼⰢⱒⰘⱈⰋⰻⰉⰹⰆⰶⰍⰽⰎⰾⰏⰿⰐⱀⰑⱁⰒⱂⰓⱃⰔⱄⰞⱎⰕⱅⰖⱆⰫⱛⰂⰲⰈⰸ/r) if ($opts{gl});
	print(tr/ⰀⰰⰁⰱⰜⱌⰝⱍⰄⰴⰅⰵⰗⱇⰃⰳⰌⰼⰢⱒⰘⱈⰋⰻⰉⰹⰆⰶⰍⰽⰎⰾⰏⰿⰐⱀⰑⱁⰒⱂⰓⱃⰔⱄⰞⱎⰕⱅⰖⱆⰫⱛⰂⰲⰈⰸ/AaBbCcĈĉDdEeFfGgĜĝHhĤĥIiJjĴĵKkLlMmNnOoPpRrSsŜŝTtUuŬŭVvZzẐẑ/r) if ($opts{GL});
	print(s/ĉ/ch/gr =~ s/ĝ/gh/gr =~ s/ĥ/hh/gr =~ s/ĵ/jh/gr =~ s/ŝ/sh/gr =~ s/ŭ/u/gr =~ s/Ĉ/CH/gr =~ s/Ĝ/GH/gr =~ s/Ĥ/HH/gr =~ s/Ĵ/JH/gr =~ s/Ŝ/SH/gr =~ s/Ŭ/U/gr) if ($opts{h});
	print(s/ch/ĉ/gr =~ s/gh/ĝ/gr =~ s/hh/ĥ/gr =~ s/jh/ĵ/gr =~ s/sh/ŝ/gr =~ s/au(?!x)/aŭ/gr =~ s/eu(?!x)/eŭ/gr =~ s/ux/ŭ/gr =~ s/CH/Ĉ/gr =~ s/GH/Ĝ/gr =~ s/HH/Ĥ/gr =~ s/JH/Ĵ/gr =~ s/SH/Ŝ/gr =~ s/AU(?!X)/AŬ/gr =~ s/EU(?!X)/EŬ/gr =~ s/UX/Ŭ/gr) if ($opts{H});
	print(s/ĉ/ch/gr =~ s/ĝ/gh/gr =~ s/ĥ/hh/gr =~ s/ĵ/jh/gr =~ s/ŝ/sh/gr =~ s/ŭ/ux/gr =~ s/Ĉ/CH/gr =~ s/Ĝ/GH/gr =~ s/Ĥ/HH/gr =~ s/Ĵ/JH/gr =~ s/Ŝ/SH/gr =~ s/Ŭ/UX/gr) if ($opts{ux});
	print(lc =~ tr/abcĉdefgĝhĥijĵklmnoprsŝtuŭvz/ɑbʦʧdɛfɡʤhxijʒklmnoprsʃtuwvz/r) if ($opts{i});
	print(tr/aɑäbʦʧdeɛfɡʤhxijʒklmnoprsʃtuwvz/aaabcĉdeefgĝhĥijĵklmnoprsŝtuŭvz/r) if ($opts{I});
	print(uc($_) =~ s!h!/H!gir =~ s/J/Y/gir =~ s/A/AA/gr =~ s/E/EH/gr =~ s/I/IY/gr =~ s/O/OH/gr =~ s/U/UX/gr =~ s/[ŭŬ]/W/gr =~ s/C/TS/gir =~ s/[ĉĈ]/CH/gr =~ s/[ĝĜ]/J/gr =~ s/[ĥĤ]/K/gr =~ s/[ĵĴ]/ZH/gr =~ s/[ŝŜ]/SH/gr) if ($opts{sam});
	print(s/([[:upper:]])/·\L\1/gr =~ tr/abcĉdefgĝhĥijĵklmnoprsŝtuŭvz/𐑨𐑚𐑔𐑗𐑛𐑧𐑓𐑜𐑡𐑣𐑙𐑦𐑢𐑠𐑒𐑤𐑫𐑵𐑩𐑐𐑮𐑕𐑖𐑑𐑪𐑘𐑝𐑟/r) if ($opts{ŝ});
	print(s/𐑲/aŭ/gr =~ s/𐑱/eŭ/gr =~ tr/𐑨𐑚𐑔𐑗𐑛𐑧𐑓𐑜𐑡𐑣𐑙𐑦𐑢𐑰𐑠𐑒𐑤𐑫𐑵𐑩𐑐𐑮𐑕𐑖𐑑𐑪𐑘𐑝𐑟𐑞/abcĉdefgĝhĥijjĵklmnoprsŝtuŭvzẑ/r =~ s/·(.)/\U\1/gr) if ($opts{Ŝ});
	print(s/([[:upper:]])/·\L\1/gr =~ s/aŭ/𐑲/gr =~ s/eŭ/𐑱/gr =~ tr/abcĉdefgĝhĥijĵklmnoprsŝtuvz/𐑨𐑚𐑔𐑗𐑛𐑧𐑓𐑜𐑡𐑣𐑙𐑦𐑢𐑠𐑒𐑤𐑫𐑵𐑩𐑐𐑮𐑕𐑖𐑑𐑪𐑝𐑟/r) if ($opts{senŭ});
	print(s/ĉ/cx/gr =~ s/ĝ/gx/gr =~ s/ĥ/hx/gr =~ s/ĵ/jx/gr =~ s/ŝ/sx/gr =~ s/ŭ/ux/gr =~ s/Ĉ/CX/gr =~ s/Ĝ/GX/gr =~ s/Ĥ/HX/gr =~ s/Ĵ/JX/gr =~ s/Ŝ/SX/gr =~ s/Ŭ/UX/gr) if ($opts{x});
	print(s/cx/ĉ/gr =~ s/gx/ĝ/gr =~ s/hx/ĥ/gr =~ s/jx/ĵ/gr =~ s/sx/ŝ/gr =~ s/ux/ŭ/gr =~ s/CX/Ĉ/gr =~ s/GX/Ĝ/gr =~ s/HX/Ĥ/gr =~ s/JX/Ĵ/gr =~ s/SX/Ŝ/gr =~ s/UX/Ŭ/gr) if ($opts{X});
}

